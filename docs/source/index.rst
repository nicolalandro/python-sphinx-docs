.. code documentation master file, created by
   sphinx-quickstart on Thu Jan  5 10:08:29 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to code's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme_link
   code/modules
