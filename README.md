[![pipeline status](https://gitlab.com/nicolalandro/python-sphinx-docs/badges/main/pipeline.svg)](https://gitlab.com/nicolalandro/python-sphinx-docs/-/commits/main)
[![coverage report](https://gitlab.com/nicolalandro/python-sphinx-docs/badges/main/coverage.svg)](https://gitlab.com/nicolalandro/python-sphinx-docs/-/commits/main)

[![gitlab pages deployed page link](https://img.shields.io/badge/Pages-Doc-orange?logo=gitlab)](https://nicolalandro.gitlab.io/python-sphinx-docs/index.html)

# Python Sphinx Docs
This is a step by step guide to make a sphinx documentation for a python repo with code into src folder.

## Create from zero
* create docs folder
```
mkdir docs
```
* create inside Dockerfile and docker-compose.yml
* build container with sphinx
```
docker-compose -f docs/docker-compose.yml build
```
* run spinxh quick start, it create a folder source inside docs folder and Makefile and make.bat
```
docker-compose -f docs/docker-compose.yml run sphinx sphinx-quickstart
sudo chmod -R 777 docs/*
```
* edit in docs/source/conf.py
```
# Import code
import os
import sys
sys.path.insert(0, os.path.abspath('../..'))
...
extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'myst_parser',
]
...
html_theme = 'sphinx_rtd_theme'
...
```
* add readme to docs by add into docs/source/index.rst under toc readme_link
* add docs/source/readme_link.md with
````
```{include} ../../README.md
```
````
* sphinx-apidoc create a folder with the .rst file that import information from code
```
docker-compose -f docs/docker-compose.yml run sphinx sphinx-apidoc -o source/code ../
sudo chmod -R 777 docs/*
```
* now edit the docs/source/index.rst by adding `code/modules` outside the toctree to import the created module

## Every day usage (Run Server)
Every day you need to do that commands to see the update in local:

```
docker-compose -f docs/docker-compose.yml build
docker-compose -f docs/docker-compose.yml run sphinx make html
docker-compose -f docs/docker-compose.yml up
# open browser at localhost:9001
```

Sometimes you want to regenerate whole code docs:
```
rm -rf docs/source/code
docker-compose -f docs/docker-compose.yml run sphinx sphinx-apidoc -o source/code ../
sudo chmod -R 777 docs/*

docker-compose -f docs/docker-compose.yml run sphinx make html
```

## Gitlab runner
Go to gitlab ci/cd and run pipeline, the page will be deployed at ...

## References
* python
* docker
* [sphinx](https://www.sphinx-doc.org/en/master/usage/quickstart.html): for documentation server
* [Document your code with sphinx](https://software.belle2.org/development/sphinx/framework/doc/atend-doctools.html)
* [sphinx rtd theme](https://sphinx-themes.org/sample-sites/sphinx-rtd-theme/): a sphinx theme
* [video tutorial document with sphinx](https://www.youtube.com/watch?v=WcUhGT4rs5o)
* [Add markdown support](https://cerodell.github.io/sphinx-quickstart-guide/build/html/markdown.html)
* [Sphinx autodoc](https://www.freecodecamp.org/news/sphinx-for-django-documentation-2454e924b3bc/)
* [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html): how to create a static server on gitlab pages
* [Sphinx gitlab pages](https://gitlab.com/pages/sphinx): how to create a CI pipeline to add sphinx to gitlab pages
